#include "cellule.hpp"
#include <iostream>

using namespace std;

Cellule::Cellule(int _valeurCellule) : valeurCellule(_valeurCellule), celluleSuivante(nullptr){}

void Cellule::setCelluleSuivante(Cellule* _celluleSuiante){
    celluleSuivante = _celluleSuiante;
}

Cellule* Cellule::getCelluleSuivante() const{
    return celluleSuivante;
}

int Cellule::getValeurCellule() const{
    return valeurCellule;
}


