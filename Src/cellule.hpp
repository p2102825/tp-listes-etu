#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP

class Cellule {
  private :
    int valeurCellule;
    Cellule* celluleSuivante;

  public : 
    // constructeur de Cellule
    Cellule(int _valeurCellule);
    
    void setCelluleSuivante(Cellule* _celluleSuiante);

    Cellule* getCelluleSuivante() const;

    int getValeurCellule() const;
} ;

#endif
