#include "liste.hpp"

#include <iostream>
#include <cassert>

using namespace std;

Liste::Liste() : debut(nullptr), fin(nullptr), tailleListe(0) {}

Liste::Liste(const Liste& autre) {
  debut = nullptr;
  fin = nullptr;
  tailleListe = 0;

  // pour parcourir la liste a copier
  const Cellule* celluleCourante = autre.tete();

  while(celluleCourante != nullptr){
    Cellule* nouvelleCellule = new Cellule(celluleCourante->getValeurCellule());

    if(debut == nullptr){
      debut = fin = nouvelleCellule;
    }else{
      fin->setCelluleSuivante(nouvelleCellule);

      fin = nouvelleCellule;
    }

    celluleCourante = celluleCourante->getCelluleSuivante();

    tailleListe++;
  }
}

Liste& Liste::operator=(const Liste& autre) {
  // on vide la liste actuelle
  while (debut != nullptr) {
    Cellule* temp = debut;
    debut = debut->getCelluleSuivante();
    delete temp;
  }

  debut = nullptr;
  fin = nullptr;
  tailleListe = 0;

  // pour parcourir la liste a copier
  const Cellule* celluleCourante = autre.tete();

  while(celluleCourante != nullptr){
    Cellule* nouvelleCellule = new Cellule(celluleCourante->getValeurCellule());

    if(debut == nullptr){
      debut = fin = nouvelleCellule;
    }else{
      fin->setCelluleSuivante(nouvelleCellule);

      fin = nouvelleCellule;
    }

    celluleCourante = celluleCourante->getCelluleSuivante();

    tailleListe++;
  }

  return *this;
}


Liste::~Liste() {
  while(debut != nullptr){
    Cellule* temp = debut;
    //on supprime les cellules en partant de la premiere 
    debut = debut->getCelluleSuivante();

    delete temp;
  }
  tailleListe = 0;
}

void Liste::ajouter_en_tete(int valeur) {
  Cellule* nouvelleCellule = new Cellule(valeur);

  if(debut == nullptr){
    debut = fin = nouvelleCellule;
  }else{
    // la nouvelle cellule pointe vers l'ancien debut
    nouvelleCellule->setCelluleSuivante(debut);
    debut = nouvelleCellule;
  }
  tailleListe++;
}

void Liste::ajouter_en_queue(int valeur) {
  Cellule* nouvelleCellule = new Cellule(valeur);

  if(debut == nullptr){
    fin = debut = nouvelleCellule;
  }else{
    // la fin pointe vers la nouvelle cellule 
    fin->setCelluleSuivante(nouvelleCellule);
    fin = nouvelleCellule;
  }
  tailleListe++;
}

void Liste::supprimer_en_tete() {
  if(debut != nullptr){
    Cellule* en_tete = debut;
    debut = en_tete->getCelluleSuivante();

    delete en_tete;

    tailleListe--;
  }else{
    cout<<"La liste est deja vide"<<endl;
  }
}

Cellule* Liste::tete() {
  return debut ;
}

const Cellule* Liste::tete() const {
  return debut ;
}

Cellule* Liste::queue() {
  return fin ;
}

const Cellule* Liste::queue() const {
  return fin ;
}

int Liste::taille() const {
  return tailleListe ;
}

Cellule* Liste::recherche(int valeur) {
  Cellule* celluleCourante = debut;
  while(celluleCourante != nullptr){
    if(celluleCourante->getValeurCellule() == valeur){
      return celluleCourante;
    }else{
      celluleCourante = celluleCourante->getCelluleSuivante();
    }
  }

  return nullptr;
}

const Cellule* Liste::recherche(int valeur) const {
  Cellule* celluleCourante = debut;
  while(celluleCourante != nullptr){
    if(celluleCourante->getValeurCellule() == valeur){
      return celluleCourante;
    }else{
      celluleCourante = celluleCourante->getCelluleSuivante();
    }
  }

  return nullptr;
}

void Liste::afficher() const {
    cout << "[ ";

    Cellule* celluleCourante = debut;
    while (celluleCourante != nullptr) {
        int val = celluleCourante->getValeurCellule();
        cout << val;

        // on ajoute un espace si ce n'est pas la dernière valeur
        if (celluleCourante->getCelluleSuivante() != nullptr) {
            cout << " ";
        }

        celluleCourante = celluleCourante->getCelluleSuivante();
    }

    cout << " ]" << endl;
}

